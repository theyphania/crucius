/*
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.theyphania.crucius.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.theyphania.crucius.Crucius;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

public class Settings
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private String version;
	private LinkedHashMap<String, Object> settings = new LinkedHashMap<>();
	
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static Settings i;
	public static Settings get() { return i; }
	public Settings() { i = this; }
	
	// -------------------------------------------- //
	// GETTERS & SETTERS
	// -------------------------------------------- //
	
	public String getVersion() { return version; }
	
	public LinkedHashMap<String, Object> getSettingsMap() { return settings; }
	
	private Settings getJsonSettings()
	{
		Settings jsonSettings = new Settings();
		jsonSettings.setVersion(this.version);
		jsonSettings.setSettingsMap(convertSettingsToJSON(this.settings));
		return jsonSettings;
	}
	
	public String getTypeOf(String setting)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		BufferedReader br = new BufferedReader(new InputStreamReader(Crucius.get().getResource("Settings" + File.separator + "settings.json")));
		Settings tempSettings = gson.fromJson(br, Settings.class);
		LinkedHashMap<String, Object> tempResourceSettings = convertSettingsToMinecraft("", tempSettings.getSettingsMap());
		
		if (!tempResourceSettings.containsKey(setting)) { return "0"; }
		
		Crucius.get().debug(tempResourceSettings.get(setting).getClass().getName());
		
		if (tempResourceSettings.get(setting).getClass().getName().contains("Boolean")) { return "boolean"; }
		if (tempResourceSettings.get(setting).getClass().getName().contains("String")) { return "string"; }
		if (tempResourceSettings.get(setting).getClass().getName().contains("Double")) { return "number"; }
		
		return "1";
	}
	
	public String getString(String setting)
	{
		if (!settings.containsKey(setting))
		{
			return null;
		}
		
		return settings.get(setting).toString();
	}
	
	public boolean getBoolean(String setting)
	{
		if (!settings.containsKey(setting))
		{
			return false;
		}
		
		return Boolean.valueOf(settings.get(setting).toString());
	}
	
	public int getInt(String setting)
	{
		if (!settings.containsKey(setting))
		{
			return 0;
		}
		
		return Integer.valueOf(settings.get(setting).toString());
	}
	
	public void setSettingsMap(LinkedHashMap<String, Object> settings)
	{
		this.settings = settings;
	}
	
	private void setVersion(String version)
	{
		this.version = version;
	}
	
	private LinkedHashMap<String, Object> convertSettingsToJSON(LinkedHashMap<String, Object> settingsMap)
	{
		LinkedHashMap<String, Object> jsonSettingsMap = new LinkedHashMap<>();
		LinkedHashMap<String, Object> level1Map = new LinkedHashMap<>();
		String level0Key = "";
		for (Entry e : settingsMap.entrySet())
		{
			String[] s = e.getKey().toString().split("\\.");
			if (s.length == 0)
			{
				Crucius.get().log(Level.WARNING, "There was a problem with the converted settings map at Settings.java:convertSettingsToJSON()");
				return null;
			}
			if (s.length == 1)
			{
				jsonSettingsMap.put(s[0], e.getValue());
				continue;
			}
			String level1Key = s[1];
			Object level1Value = e.getValue();
			if (!level0Key.equalsIgnoreCase(s[0]))
			{
				level0Key = s[0];
				level1Map.clear();
			}
			level1Map.put(level1Key, level1Value);
			jsonSettingsMap.put(level0Key, level1Map);
		}
		return jsonSettingsMap;
	}
	
	private LinkedHashMap<String, Object> convertSettingsToMinecraft(String key, Map<String, Object> m)
	{
		LinkedHashMap<String, Object> minecraftSettingsMap = new LinkedHashMap<>();
		for (Entry e : m.entrySet())
		{
			Crucius.get().debug(e.getValue().getClass().getName());
			if (e.getValue().getClass().getName().contains("Map"))
			{
				minecraftSettingsMap.putAll(convertSettingsToMinecraft(e.getKey() + ".", (Map<String, Object>) e.getValue()));
			}
			else
			{
				minecraftSettingsMap.put(key + e.getKey(), e.getValue());
			}
		}
		return minecraftSettingsMap;
	}
	
	public void set(String setting, Object value)
	{
		settings.put(setting, value);
	}
	
	public void write() { write("settings.json"); }
	
	public void write(String fileName)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		String jsonString = gson.toJson(getJsonSettings());
		try
		{
			FileWriter fileWriter = new FileWriter(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + fileName);
			fileWriter.write(jsonString);
			fileWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Settings read() {	return read(false);	}
	
	public static Settings read(boolean resource)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		try
		{
			File tempFile = new File(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + "tmp.json");
			if (!tempFile.exists()) { Files.copy(Crucius.get().getResource("Settings" + File.separator + "settings.json"), tempFile.toPath()); }
			if (!resource) { tempFile =  new File(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + "settings.json"); }
			BufferedReader br = new BufferedReader(new FileReader(tempFile));
			Settings tempSettings = gson.fromJson(br, Settings.class);
			tempSettings.setSettingsMap(tempSettings.convertSettingsToMinecraft("", tempSettings.getSettingsMap()));
			Files.deleteIfExists(Paths.get(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + "tmp.json"));
			return tempSettings;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static Settings update()
	{
		// Settings from Resources
		Settings tmpSettingsLocal = read(true);
		
		// Settings from File
		Settings tmpSettingsExternal = read();
		
		if (tmpSettingsExternal == null || tmpSettingsLocal == null) { return null;	}
		
		tmpSettingsExternal.write("settings.backup");
		
		LinkedHashMap<String, Object> tmpSettingsMap = tmpSettingsLocal.getSettingsMap();
		
		//tmpSettingsMap.putAll(tmpSettingsExternal.getSettingsMap());
		
		for (Entry<String, Object> e : tmpSettingsExternal.getSettingsMap().entrySet())
		{
			if (!tmpSettingsMap.containsKey(e.getKey())) { continue; }
			tmpSettingsMap.put(e.getKey(), e.getValue());
		}
		
		tmpSettingsLocal.setSettingsMap(tmpSettingsMap);
		tmpSettingsLocal.write();
		return tmpSettingsLocal;
	}
}
