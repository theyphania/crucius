/*
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.theyphania.crucius.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.theyphania.crucius.Crucius;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Lang
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private String version;
	private LinkedHashMap<String, String> locale = new LinkedHashMap<>();
	private LinkedHashMap<Messages, String> lang = new LinkedHashMap<>();
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static Lang i;
	public static Lang get() { return i; }
	public Lang() { i = this; }
	
	// -------------------------------------------- //
	// GETTERS & SETTERS
	// -------------------------------------------- //
	
	public String getVersion() { return version; }
	
	public String getLocaleCode() { return locale.get("code"); }
	
	public String getLocaleName() { return locale.get("name"); }
	
	static public String getConvertedPlayerLocale(Player player)
	{
		return player.getLocale().split("_")[0];
	}
	
	private LinkedHashMap<Messages, String> getLangMap()
	{
		LinkedHashMap<Messages, String> tmp = new LinkedHashMap<>();
		
		for (Entry<Messages, String> e : this.lang.entrySet())
		{
			tmp.put(e.getKey(), e.getValue().replace("\\\\u0026", "&"));
		}
		
		return tmp;
	}
	
	private void setLangMap(LinkedHashMap<Messages, String> lang) { this.lang = lang; }
	
	public String getMessage(Messages message) { return getMessage(message, null); }
	
	public String getMessage(Messages message, String[] paramArrayOfString) {
		
		String rawStr;
		if (!lang.containsKey(message))
		{
			Lang tmp = this.getLocaleCode().equalsIgnoreCase("en") ? Lang.read(true, "en") : Lang.read("en");
			if (tmp == null) return null;
			rawStr = tmp.getLangMap().get(message);
		}
		else
		{
			rawStr = lang.get(message);
		}
		rawStr = rawStr.replaceAll("\\\\", "\\\\\\\\");
		String str = ChatColor.translateAlternateColorCodes('&', rawStr);
		
		if (paramArrayOfString == null) {
			
			return str;
			
		}
		
		if (paramArrayOfString.length == 0) {
			
			return str;
			
		}
		
		for (int i = 0; i < paramArrayOfString.length; i++) {
			
			str = str.replace("{" + i + "}", paramArrayOfString[i]);
			
		}
		
		return str;
		
	}
	
	public void write() { write(this.locale.get("code") + ".json"); }
	
	public void write(String fileName)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls().create();
		String jsonString = gson.toJson(this);
		try
		{
			BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Languages" + File.separator + fileName), StandardCharsets.UTF_8));
			fileWriter.write(jsonString);
			fileWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Lang read(String locale) { return read(false, locale); }
	
	public static Lang read(boolean resource, String locale)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls().create();
		File filePath = new File(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Languages");
		File[] listOfFiles = filePath.listFiles();
		boolean localeExists = false;
		assert listOfFiles != null: "Language files got deleted. Restart the server to solve the problem.";
		for (File f : listOfFiles)
		{
			if (f.getName().equalsIgnoreCase(locale + ".json")) localeExists = true;
		}
		if (!localeExists) locale = "en";
		try
		{
			File tempFile = new File(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Languages" + File.separator + "tmp.json");
			if (!tempFile.exists()) { Files.copy(Crucius.get().getResource("Languages" + File.separator + locale + ".json"), tempFile.toPath()); }
			if (!resource) { tempFile =  new File(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Languages" + File.separator + locale + ".json"); }
			BufferedReader br = new BufferedReader(new InputStreamReader( new FileInputStream(tempFile), StandardCharsets.UTF_8));
			Lang tempLang = gson.fromJson(br, Lang.class);
			Files.deleteIfExists(Paths.get(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Languages" + File.separator + "tmp.json"));
			Crucius.get().debug(tempLang.getMessage(Messages.SPACER_UPPER) + " " + resource);
			return tempLang;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lang update(String locale)
	{
		// Settings from Resources
		Lang tmpLangLocal = Lang.read(true, locale);
		
		// Settings from File
		Lang tmpLangExternal = Lang.read(locale);
		
		if (tmpLangExternal == null || tmpLangLocal == null) { return null; }
		
		tmpLangExternal.write(locale + ".backup");
		
		LinkedHashMap<Messages, String> tmpLangMap = tmpLangLocal.getLangMap();
		
		//tmpSettingsMap.putAll(tmpSettingsExternal.getSettingsMap());
		
		for (Entry<Messages, String> e : tmpLangExternal.getLangMap().entrySet())
		{
			if (!tmpLangMap.containsKey(e.getKey())) { continue; }
			tmpLangMap.put(e.getKey(), e.getValue());
		}
		
		tmpLangLocal.setLangMap(tmpLangMap);
		tmpLangLocal.write();
		return tmpLangLocal;
	}
	
}
