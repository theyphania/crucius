/*
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.theyphania.crucius.listeners;

import de.theyphania.crucius.Crucius;
import de.theyphania.crucius.util.Lang;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static de.theyphania.crucius.util.Messages.CRUCIUS;
import static de.theyphania.crucius.util.Messages.INCORRECT_USAGE;
import static de.theyphania.crucius.util.Messages.NOT_ENOUGH_ARGUMENTS;
import static de.theyphania.crucius.util.Messages.NO_PERMISSION;
import static de.theyphania.crucius.util.Messages.SPACER_LOWER;
import static de.theyphania.crucius.util.Messages.SPACER_UPPER;

public class JoinListener implements Listener
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private Crucius plugin = Crucius.get();
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static JoinListener i;
	public static JoinListener get() { return i; }
	public JoinListener()
	{
		i = this;
		plugin.log("Registering listener JoinListener (PlayerJoinEvent)");
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	// -------------------------------------------- //
	// LISTENERS
	// -------------------------------------------- //
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		if (plugin.getSettings().getBoolean("debug"))
		{
			Bukkit.getScheduler().runTaskLater(plugin, () -> {
				Player p = e.getPlayer();
				Lang lang = Lang.read(Lang.getConvertedPlayerLocale(p));
				
				p.sendMessage("[Crucius] Debug mode is activated. Deactivate it with '/crucius settings debug'.");
				p.sendMessage("[Crucius] This server uses version '" + plugin.getDescription().getVersion() + "'.");
				p.sendMessage("[Crucius] Player name: " + p.getName());
				p.sendMessage("[Crucius] Player locale: " + p.getLocale());
				p.sendMessage("[Crucius] Converted player locale: " + Lang.getConvertedPlayerLocale(p));
				p.sendMessage("[Crucius] Player IP: " + p.getAddress().toString());
				p.sendMessage("[Crucius] Debug output:");
				
				p.sendMessage(lang.getMessage(SPACER_UPPER, new String[] { lang.getMessage(CRUCIUS).toUpperCase() }));
				p.sendMessage(lang.getMessage(NO_PERMISSION));
				p.sendMessage(lang.getMessage(INCORRECT_USAGE, new String[] { lang.getMessage(NOT_ENOUGH_ARGUMENTS) }));
				p.sendMessage(lang.getMessage(SPACER_LOWER, new String[] { lang.getMessage(CRUCIUS).toUpperCase() }));
			}, 100);
		}
	}
}
