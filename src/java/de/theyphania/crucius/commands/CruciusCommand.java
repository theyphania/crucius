/*
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.theyphania.crucius.commands;

import de.theyphania.crucius.Crucius;
import de.theyphania.crucius.util.Lang;
import de.theyphania.crucius.util.Messages;
import de.theyphania.crucius.util.Settings;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.CommandPermission;
import dev.jorel.commandapi.arguments.Argument;
import dev.jorel.commandapi.arguments.BooleanArgument;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import dev.jorel.commandapi.arguments.IntegerArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import org.apache.commons.lang.StringUtils;
import org.bukkit.entity.Player;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

public class CruciusCommand
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private Crucius plugin = Crucius.get();
	private LinkedHashMap<String, Argument> arguments = new LinkedHashMap<>();
	private String cmd = "crucius";
	private String[] aliases = { "cr", "cruc" };
	private CommandPermission permission = CommandPermission.fromString("crucius.basecommand");
	private String code = "en";
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static CruciusCommand i;
	public static CruciusCommand get() { return i; }
	public CruciusCommand() {	i = this; }
	
	// -------------------------------------------- //
	// COMMANDS
	// -------------------------------------------- //
	
	@SuppressWarnings("Duplicates")
	public void registerCommand()
	{
		
		// -------------------------------------------- //
		// /crucius
		// -------------------------------------------- //
		
		new CommandAPICommand(cmd)
			.withAliases(aliases)
			.withPermission(permission)
			.executes((sender, args) ->
			{
				if (sender instanceof Player)
				{
					Player p = (Player) sender;
					code = Lang.getConvertedPlayerLocale(p);
				}
				Lang lang = Lang.read(code);
				sender.sendMessage(lang.getMessage(Messages.SPACER_UPPER, new String[]{lang.getMessage(Messages.CRUCIUS).toUpperCase()}));
				sender.sendMessage(lang.getMessage(Messages.INFO1, new String[]{plugin.getDescription().getName(), plugin.getDescription().getVersion()}));
				sender.sendMessage(lang.getMessage(Messages.INFO2, new String[]{"" + plugin.getDescription().getAuthors()}));
				sender.sendMessage(lang.getMessage(Messages.INFO3, new String[]{plugin.getDescription().getWebsite()}));
				if (sender.hasPermission("crucius.basecommand.help"))
					sender.sendMessage(lang.getMessage(Messages.INFO4, new String[]{"/" + cmd + " help"}));
				sender.sendMessage(lang.getMessage(Messages.SPACER_LOWER, new String[]{lang.getMessage(Messages.CRUCIUS).toUpperCase()}));
			}).register();
		
		// -------------------------------------------- //
		// /crucius settings
		// -------------------------------------------- //
		
		Settings settings = plugin.getSettings();
		arguments.put("args0Settings", new LiteralArgument("settings"));
		permission = CommandPermission.fromString("crucius.basecomand.settings");
		new CommandAPICommand(cmd)
			.withAliases(aliases)
			.withPermission(permission)
			.withArguments(arguments)
			.executes((sender, args) ->
			{
				if (sender instanceof Player)
				{
					Player p = (Player) sender;
					code = Lang.getConvertedPlayerLocale(p);
				}
				Lang lang = Lang.read(code);
				sender.sendMessage(lang.getMessage(Messages.SPACER_UPPER, new String[]{lang.getMessage(Messages.CRUCIUS).toUpperCase()}));
				for (Entry e : settings.getSettingsMap().entrySet())
				{
					sender.sendMessage("§b" + StringUtils.capitalize(e.getKey().toString()) + ": §6" + e.getValue());
				}
				sender.sendMessage(lang.getMessage(Messages.SPACER_LOWER, new String[]{lang.getMessage(Messages.CRUCIUS).toUpperCase()}));
			}).register();
		arguments.clear();
		
		// -------------------------------------------- //
		// /crucius settings <setting> <value>
		// -------------------------------------------- //
		
		for (Entry entry : plugin.getSettings().getSettingsMap().entrySet())
		{
			arguments.put("args0Settings", new LiteralArgument("settings"));
			arguments.put("args1" + entry.getKey(), new LiteralArgument(entry.getKey().toString()));
			switch (plugin.getSettings().getTypeOf(entry.getKey().toString()))
			{
				case "boolean":
					arguments.put("args2Value", new BooleanArgument());
					break;
				case "number":
					arguments.put("args2Value", new IntegerArgument());
					break;
				case "string":
					arguments.put("args2Value", new GreedyStringArgument());
					break;
				case "0":
					plugin.log(Level.SEVERE, "No such setting found. Initiate Plugin shutdown for further investigation.");
					plugin.suicide();
				default:
					plugin.log(Level.SEVERE, "There was a problem with getting the type of your setting. Initiate Plugin shutdown for further investigation.");
					plugin.suicide();
			}
			new CommandAPICommand(cmd)
				.withAliases(aliases)
				.withPermission(CommandPermission.fromString("crucius.basecomand.settings"))
				.withArguments(arguments)
				.executes((sender, args) ->
				{
					if (sender instanceof Player)
					{
						Player p = (Player) sender;
						code = Lang.getConvertedPlayerLocale(p);
					}
					Lang lang = Lang.read(code);
					plugin.getSettings().set(entry.getKey().toString(), args[0]);
					sender.sendMessage(lang.getMessage(Messages.COMMAND_CRUCIUS_SETTINGS, new String[]{entry.getKey().toString(), args[0].toString(), "settings.json"}));
				}).register();
			arguments.clear();
		}
	}
}
