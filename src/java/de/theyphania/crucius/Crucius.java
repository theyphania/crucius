/*
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.theyphania.crucius;

import de.theyphania.crucius.commands.CruciusCommand;
import de.theyphania.crucius.listeners.JoinListener;
import de.theyphania.crucius.util.Lang;
import de.theyphania.crucius.util.Settings;
import dev.jorel.commandapi.CommandAPI;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Crucius extends CruciusPlugin
{
	
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private static Crucius i;
	private Settings settings = new Settings();
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	private final List<String> langList = new ArrayList<>();
	
	public Crucius() { i = this; }
	
	public static Crucius get() { return i; }
	
	// -------------------------------------------- //
	// ENABLE
	// -------------------------------------------- //
	
	@Override
	public void onEnableInner()
	{
		langList.add("de");
		langList.add("en");
		
		loadResources();
		registerListeners();
		registerCommands();
		
		if (getSettings().getBoolean("stats"))
		{
			log("The collection of metrics is enabled. You can disable it any time via '/crucius settings stats false'");
			int pluginID = 3695;
			Metrics metrics = new Metrics(this, pluginID);
		}
	}
	
	@Override
	public void onDisable()
	{
		CommandAPI.unregister("crucius", true);
		getSettings().write();
		log("Plugin disabled! Goodbye!");
	}
	
	private void registerListeners()
	{
		new JoinListener();
	}
	
	private void registerCommands()
	{
		new CruciusCommand().registerCommand();
	}
	
	private void loadResources()
	{
		int max = 3;
		log("STARTED", "Loading resources...");
		log("STEP 1/" + max, "Loading plugin directories...");
		String folderPlugin = getDataFolder().getAbsolutePath();
		String folderLanguages = folderPlugin + File.separator + "Languages";
		String folderSettings = folderPlugin + File.separator + "Settings";
		String folderModules = folderPlugin + File.separator + "Modules";
		String folderThirdParty = folderModules + File.separator + "Third Party";
		List<File> fileList = new ArrayList<>();
		
		fileList.add(new File(folderPlugin));
		fileList.add(new File(folderLanguages));
		fileList.add(new File(folderSettings));
		fileList.add(new File(folderModules));
		fileList.add(new File(folderThirdParty));
		
		for (File f : fileList)
		{
			if (f.mkdirs()) log("STEP 1/" + max, f.getName() + " directory does not exist and is therefore created.");
		}
		log("STEP 1/" + max, "All directories loaded successfully.");
		log("STEP 2/" + max, "Loading plugin settings...");
		loadSettings();
		debug("DEBUG mode is activated. Please be aware of unintended behaviour. Use '/crucius settings debug false' to disable it.");
		log("STEP 2/" + max, "Successfully loaded " + settings.getSettingsMap().size() + " settings on version (" + settings.getVersion() + ").");
		log("STEP 3/" + max, "Loading plugin languages...");
		loadLang(langList, true);
		File[] fileList1 = Objects.requireNonNull(new File(folderLanguages).listFiles());
		List<File> langFiles = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		Lang tmpLang;
		for (File f : fileList1)
		{
			if (f.getName().contains("json"))
			{
				langFiles.add(f);
				tmpLang = Lang.read(f.getName().substring(0, 2));
				sb.append(tmpLang.getLocaleName()).append(" (v").append(tmpLang.getVersion()).append("), ");
			}
		}
		log("STEP 3/" + max, "There are " + langFiles.size() + " loaded languages: ");
		log("STEP 3/" + max, sb.toString().substring(0, sb.toString().length() - 2));
		log("FINISHED", "All resources loaded successfully.");
	}
	

	
	public Settings getSettings()
	{
		return settings;
	}
	
	private void settingsWatcher()
	{
		Bukkit.getScheduler().runTaskAsynchronously(this, () ->
		{
			Path settingsPath = Paths.get(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Settings");
			try
			{
				WatchService watcher = settingsPath.getFileSystem().newWatchService();
				settingsPath.register(watcher, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
				
				WatchKey key;
				boolean scnd = false; //Let only every second watcher event fire!
				while ((key = watcher.take()) != null)
				{
					for (WatchEvent<?> event : key.pollEvents())
					{
						if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY && event.context().toString().equalsIgnoreCase("settings.json"))
						{
							if (scnd)
							{
								scnd = false;
							}
							else
							{
								
								log("File 'settings.json' was modified. Applying changes now...");
								this.settings = Settings.read();
								log("Changes successfully applied.");
								scnd = true;
							}
						}
						if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE && event.context().toString().equalsIgnoreCase("settings.json"))
						{
							loadSettings();
						}
					}
					key.reset();
					
				}
				
			}
			catch (Exception e)
			{
				error(e);
			}
		});
	}
	
	private void loadSettings()
	{
		File settingsFile = new File(getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + "settings.json");
		if (!settingsFile.exists())
		{
			try
			{
				Files.copy(getResource("Settings" + File.separator + "settings.json"), settingsFile.toPath());
			}
			catch (Exception e)
			{
				error(e);
			}
		}
		
		Settings tmpRsSttngs = Settings.read(true);
		settings = Settings.read();
		
		if (!Objects.requireNonNull(tmpRsSttngs).getVersion().equalsIgnoreCase(settings.getVersion()))
		{
			settings = Settings.update();
		}
		
	}
	
	private void langWatcher()
	{
		
		File readme = new File(getDataFolder().getAbsolutePath() + File.separator + "Languages" + File.separator + "README.txt");
		try
		{
			if (readme.exists()) Files.delete(readme.toPath());
			Files.copy(getResource("Languages" + File.separator + "README.txt"), readme.toPath());
		}
		catch (IOException e)
		{
			error(e);
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(this, () ->
		{
			Path langPath = Paths.get(Crucius.get().getDataFolder().getAbsolutePath() + File.separator + "Languages");
			try
			{
				WatchService watcher = langPath.getFileSystem().newWatchService();
				langPath.register(watcher, StandardWatchEventKinds.ENTRY_DELETE);
				
				WatchKey key;
				while ((key = watcher.take()) != null)
				{
					for (WatchEvent<?> event : key.pollEvents())
					{
						if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE && event.context().toString().equalsIgnoreCase("en.json"))
						{
							List<String> tempLangList = new ArrayList<>();
							tempLangList.add("en");
							loadLang(tempLangList);
							tempLangList.clear();
							log("Restored default language file. Please do not try to delete it, thank you.");
						}
					}
					key.reset();
				}
			}
			catch (Exception e)
			{
				error(e);
			}
		});
	}
	
	private void loadLang(List<String> lang)
	{
		loadLang(lang, false);
	}
	
	private void loadLang(List<String> lang, boolean watcher)
	{
		if (lang.isEmpty()) return;
		String path = getDataFolder().getAbsolutePath() + File.separator + "Languages" + File.separator;
		File langFile;
		for (String s : lang)
		{
			langFile = new File(path + s + ".json");
			this.debug("." + s + ".");
			if (!langFile.exists())
			{
				try
				{
					Files.copy(getResource("Languages" + File.separator + s + ".json"), langFile.toPath());
				}
				catch (IOException e)
				{
					error(e);
				}
			}
		}
		
		for (String s : lang)
		{
			Lang tmpLangR = Lang.read(true, s);
			Lang tmpLangE = Lang.read(s);
			if (!Objects.requireNonNull(tmpLangR).getVersion().equalsIgnoreCase(tmpLangE.getVersion()))
			{
				Lang.update(s);
			}
		}
		
		
		if (watcher) langWatcher();
	}
}
