# Crucius

![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/theyphania/crucius?logo=atlassian&style=for-the-badge)
[![javadoc](https://javadoc.io/badge2/de.theyphania/Crucius/javadoc.svg?style=for-the-badge)](https://javadoc.io/doc/de.theyphania/Crucius)
![Maven Central](https://img.shields.io/maven-central/v/de.theyphania/Crucius?style=for-the-badge)
![License](https://img.shields.io/badge/license-GPL%20v3-blue?style=for-the-badge)

**Version:** 0.2.0

Crucius is a general purpose Minecraft plugin that allows server owners to use its direct and third party modules to enhance their server.

## Prerequisites
Before you begin, ensure you have met the following requirements:
- This version of Crucius supports [Spigot](https://www.spigotmc.org/) versions 1.3 - 1.16.1
- You have read [the documentation]().

## Installing Crucius
To install Crucius on your server, follow these steps:
1. Download the latest version of [JorelAli's 1.13 CommandAPI](https://www.jorel.dev/1.13-Command-API/) and place it into your `plugins` folder.
2. Download the latest version of [Crucius]() and place it into your `plugins` folder.
3. **Restart** your server. Server reloads as well as plugin reloads (PlugMan, etc.) will **not** work!

## Using Crucius
To use Crucius, follow these steps:
1. Complete the in-game setup wizard via `/crucius setup`.
2. Download any [supported module]() from its respective source and place it inside Crucius' `Modules` folder.
3. **Restart** your server to load and enable all modules.

## Contributing to Crucius
To contribute to Crucius, follow these steps:
1. Fork this repository.
2. Create a branch: `git checkout -b <branch-name>`.
3. Make your changes, test and then commit them: `git commit -m '<commit-message>'`
4. Push to the `develop` branch: `git push origin develop`.
5. Create a pull request.

Please see the Atlassian documentation for [forking](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/) and [creating pull requests](https://support.atlassian.com/bitbucket-cloud/docs/create-a-pull-request/).

## Writing your own modules for Crucius
To write your own modules for Crucius, follow these steps:
1. Familiarize yourself with Crucius' [module API]().
2. Read the corresponding section inside our [documentation]().
3. Follow the contributing guidelines above.

## Contributors
Thanks to the following people who have contributed to this project:
- None so far, be the first :)

## Contact
If you want to contact me [write me a mail](mailto://hello@loapu.dev)

## License
This project is licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html)
